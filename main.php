<?php
/*
Plugin Name: Reservations
Description: A plugin that allows keeping record of reservations
Version:     1.0
Author:      Javier Cubides
Author URI:  http://jcf.design
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
define('RESFILE', __FILE__);
$dir = plugin_dir_path(	RESFILE );

include_once $dir . 'inc/init.php';
include_once $dir . 'inc/admin.php';
include_once $dir . 'inc/utils.php';
include_once $dir . 'inc/widget.php';

/**/