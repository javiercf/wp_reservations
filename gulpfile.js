var gulp = require('gulp'),
	gulpLoadPlugins = require('gulp-load-plugins'),
	$ = gulpLoadPlugins();

gulp.task('sass', function (){
	return gulp.src('scss/**/*.scss')
	.pipe($.sourcemaps.init())
	.pipe($.sass())
	.pipe($.autoprefixer())
	.pipe($.rename({extname:'.min.css'}))
	.pipe($.sourcemaps.write('.'))
	.pipe(gulp.dest('./css'));
});

gulp.task('js', function (){
	return gulp.src('./js/*.js')
	.pipe($.sourcemaps.init())
	.pipe($.jshint())
	.pipe($.uglify())
	.pipe($.rename({
		extname: '.min.js'
	}))
	.pipe($.sourcemaps.write('.'))
	.pipe(gulp.dest('./js/min'))
});

gulp.task('default', ['sass', 'js'], function(){
	gulp.watch('scss/**/*.scss', ['sass']);
	gulp.watch('js/*.js', ['js']);
});