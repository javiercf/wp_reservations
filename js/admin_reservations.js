(function($) {

	$(function() { // document ready

		var events = [];

		$.ajax({
			type: 'get',
			url: ajaxfilter.ajaxurl,
			data: {action:'reservationManagement'},
			success: function(result){
				reservations = JSON.parse(result);
				reservations.forEach(function(obj){
					var reservation = {};
					reservation.title = obj.name;
					reservation.start = obj.arrival;
					reservation.end = obj.checkout;
					reservation.allDay = true;
					events.push(reservation);

					$('#reservations_cal').fullCalendar({
						header: {
							left: 'prev,next today',
							center: 'title',
							right: 'month,agendaWeek,agendaDay'
						},
						defaultDate: new Date,
						defaultView: 'month',
						editable: true,
						eventDurationEditable: true,
						events: events
					});

				});
			}
		});


	});

})(jQuery);