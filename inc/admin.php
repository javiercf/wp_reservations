<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

add_action( 'admin_menu', 'my_plugin_menu' );

function my_plugin_menu() {
	add_options_page( 'Reservation Options', 'Reservaciones', 'manage_options', 'reservation-settings', 'manage_reservations' );
	add_options_page( 'Reservation Options', 'Ajustes Reservaciones', 'manage_options', 'reservations', 'my_plugin_options' );
}

function my_plugin_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include_once plugin_dir_path( RESFILE ) . 'templates/admin.php';
}

function manage_reservations(){
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include_once plugin_dir_path( RESFILE ) . 'templates/reservations.php';
}

/**/
