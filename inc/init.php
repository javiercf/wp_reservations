<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function jal_install(){
	global $wpdb;

	$table_reservations = $wpdb->prefix . 'reservations';
	$table_rooms = $wpdb->prefix . 'rooms';

	$charset_collate = $wpdb->get_charset_collate();
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$sql = "CREATE TABLE $table_rooms (
			id mediumint(9) NOT NULL AUTO_INCREMENT  PRIMARY KEY,
			name varchar(255) NOT NULL,
			price mediumint(20) NOT NULL
		) $charset_collate;

		CREATE TABLE $table_reservations (
			id mediumint(9) NOT NULL AUTO_INCREMENT  PRIMARY KEY,
			arrival datetime NOT NULL,
			checkout datetime NOT NULL,
			name varchar(255) NOT NULL,
			email varchar(255) NOT NULL,
			status varchar(255) NOT NULL,
			room_id mediumint(9) NOT NULL,
			room_name varchar(255) NOT NULL,
			KEY room_id (room_id)
		) $charset_collate;";

	dbDelta( $sql );
}

register_activation_hook( RESFILE, 'jal_install' );

/**/