<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function reservationManagement(){
	global $wpdb;

	$table_reservations = $wpdb->prefix . 'reservations';
	$table_rooms = $wpdb->prefix . 'rooms';

	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		if(isset($_POST['arrival']) && isset($_POST['checkout']) && isset($_POST['res_room']) && isset($_POST['name']) && isset($_POST['email'])){
			$wpdb->show_errors();
			$checkout = date('Y-m-d H:i:s', strtotime($_POST['checkout']));
			$arrival = date('Y-m-d H:i:s', strtotime($_POST['arrival']));
			$room_id = (int)$_POST['res_room'];
			$room_name = $wpdb->get_results("SELECT name FROM $table_rooms WHERE id=$room_id");
			$name = $_POST['name'];
			$email = $_POST['email'];
			$sql = "INSERT INTO $table_reservations (arrival, checkout, name, email, status, room_id, room_name) VALUES (%s, %s, %s, %s, %s, %s, %s)";
			$wpdb->query($wpdb->prepare($sql, $arrival, $checkout, $name, $email, 'pending', $room_id, $room_name));
			echo 'reservation saved';
			die();
		} else{
			echo 'error';
			die();
		}

	} else{
		if(!isset($_GET['arrival']) && !isset($_GET['checkout'])){
			$sql = "SELECT * FROM $table_reservations";
			$results = $wpdb->get_results( $sql );
			echo json_encode($results);
			die();

		} else{
			$checkout = date('Y-m-d H:i:s', strtotime($_GET['checkout']));
			$arrival = date('Y-m-d H:i:s', strtotime($_GET['arrival']));
			$sql = "SELECT r.* FROM $table_rooms r WHERE r.id NOT IN ( SELECT b.room_id FROM $table_reservations b WHERE NOT (b.checkout < '$arrival' OR b.arrival > '$checkout')) ORDER BY r.id;";
			$results = $wpdb->get_results( $sql );
			echo json_encode($results);
			die();

		}

	}

}

add_action('wp_ajax_nopriv_reservationManagement', 'reservationManagement');
add_action('wp_ajax_reservationManagement', 'reservationManagement');



function reservations_scripts() {

	wp_enqueue_script( 'moment', plugin_dir_url( RESFILE ) . 'js/libs/moment.min.js' );
	wp_enqueue_script( 'fullcal', plugin_dir_url( RESFILE ) . 'js/libs/fullcalendar.min.js', ['jquery', 'moment'] );
	wp_enqueue_script('reservations', plugin_dir_url(RESFILE) . 'js/min/admin_reservations.min.js', ['fullcal']);
	wp_enqueue_style('fullcal-css', plugin_dir_url(RESFILE) . 'css/fullcalendar.min.css' );
	wp_localize_script( 'reservations', 'ajaxfilter', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
		));
}

function client_scripts(){
	wp_enqueue_style('reservation_css', plugin_dir_url(RESFILE) . 'css/reservations_main.min.css');
	wp_enqueue_script('reservation_js', plugin_dir_url(RESFILE) . 'js/min/reservations.min.js', ['jquery']);
	wp_localize_script('reservation_js', 'ajaxfilter', array(
		'ajaxurl' => admin_url('admin-ajax.php')
		));
}


add_action('wp_enqueue_scripts', 'client_scripts');
add_action( 'admin_enqueue_scripts', 'reservations_scripts' );

/**/