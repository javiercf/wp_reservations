<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Adds Res_Widget widget.
 */
class Res_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'reservations_widget', // Base ID
			__( 'Reservations', 'text_domain' ), // Name
			array( 'description' => __( 'A Reservation making widget', 'text_domain' ), ) // Args
			);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		$html = '<div class="reservation_module_widget">';
		$html .= '<form name="reservation_widget" method="post" id="res_form" action="">';
		$html .= '<div class="res_form-row">';
		$html .= '<div class="res_form-group">';
		$html .= '<label for="res_arrival">';
		$html .= __('Arrival', 'text_domain');
		$html .= '</label>';
		$html .= '<input name="arrival" type="date" id="res_arrival"/>';
		$html .= '</div>';
		$html .= '<div class="res_form-group">';
		$html .= '<label for="res_checkout">';
		$html .= __('Checkout', 'text_domain');
		$html .= '</label>';
		$html .= '<input name="checkout" type="date" id="res_checkout" disabled />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div class="res_form-row" id="room_data"></div>';
		$html .= '<div class="res_form-row">';
		$html .= '<div class="res_form-group hidden">';
		$html .= '<label for="res_name">';
		$html .= __('Name', 'text_domain');
		$html .= '</label>';
		$html .= '<input name="name" id="res_name" />';
		$html .= '</div>';
		$html .= '<div class="res_form-group hidden">';
		$html .= '<label for="res_email">';
		$html .= __('Email', 'text_domain');
		$html .= '</label>';
		$html .= '<input name="email" id="res_email" type="email" />';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div class="res_form-row"><div class="res_form-group">';
		$html .= '<input type="submit" value="'. __('Confirm Reservation', 'text_domain') .'"/>';
		$html .= '</div></div>';
		$html .= '</form>';
		$html .= '</div>';
		echo $html;
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'text_domain' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Foo_Widget

function register_res_widget() {
	register_widget("Res_Widget");
}

add_action( 'widgets_init', 'register_res_widget' );

/**/