(function($) {
	$(document).on('submit', '#res_form', function(e){
		e.preventDefault();
		var data = $(this).serialize()+'&action=reservationManagement';
		console.log(data);
		$.ajax({
			type:'post',
			url:ajaxfilter.ajaxurl,
			data: data,
			success: function(response){
				console.log(response);
			}
		});
	});


	$(document).on('change', 'input[name="arrival"]', function(){
		if (this.value!=''){
			$('input[name="checkout"]').removeAttr('disabled');
		}
	});
	$(document).on('change', 'input[name="checkout"]', function(){
		if (this.value!='' && this.value > $('input[name="arrival"]').val()){
			console.log('valid');
			var arrival = $('input[name="arrival"]').val(),
			checkout = this.value;
			$.ajax({
				type: 'get',
				url: ajaxfilter.ajaxurl,
				data: {action:'reservationManagement', arrival:arrival, checkout:checkout},
				success: function(result){
					var rooms = JSON.parse(result);
					var select = '';
					var options = '';
					rooms.forEach(function(room){
						options += '<option value="' + room.id + '">' + room.name + '</option>';
					});
					select = '<div class="res_form-group">';
					select += '<label for="res_room">Room</label>';
					select += '<select name="res_room" id="res_room">';
					select += options;
					select += '</select>';
					select += '</div>';
					select += '<div class="res_form-group">';
					select += '<input type="radio" name="res_payment" id="res_payment_1" value="part"/>';
					select += '<label for="res_payment_1">Pay 1 night</label>';
					select += '<input type="radio" name="res_payment" id="res_payment_2" value="full"/>';
					select += '<label for="res_payment_2">Pay full</label>';
					select += '</div>';


					if(rooms.length>0){
						if($('#res_room').length > 0){
							$('#res_room').html(options);
						} else{
							$('#room_data').append(select);
							$('.res_form-group.hidden').removeClass('hidden');
						}
					}
				}
			});
		} else{
			console.log('invalid');
		}
	});

	$(document).on('change', '#res_room', function(){
		$('#res_name', '#res_email').removeAttr('disabled').addClass('res_show');
	});
	
})(jQuery);
